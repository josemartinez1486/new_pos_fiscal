﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.AxEpsonFPHostControl1 = New AxEpsonFPHostControlX.AxEpsonFPHostControl()
        Me.btn_no_venta = New System.Windows.Forms.Button()
        Me.btn_libro_de_venta = New System.Windows.Forms.Button()
        Me.btn_reconectar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btn_fact_con_exoneracion_de_impuestos = New System.Windows.Forms.Button()
        Me.btn_fact_credito_fiscal = New System.Windows.Forms.Button()
        Me.cmd_fact_consumidor_final = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btn_nc_exoneracion = New System.Windows.Forms.Button()
        Me.btn_nc_credito_fiscal = New System.Windows.Forms.Button()
        Me.btn_nc_consumidor_final = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btn_cierre_z = New System.Windows.Forms.Button()
        Me.btn_informe_x = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btn_por_fecha = New System.Windows.Forms.Button()
        Me.btn_por_numero = New System.Windows.Forms.Button()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.txt_z_1 = New System.Windows.Forms.TextBox()
        Me.txt_z_2 = New System.Windows.Forms.TextBox()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        CType(Me.AxEpsonFPHostControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'AxEpsonFPHostControl1
        '
        Me.AxEpsonFPHostControl1.Enabled = True
        Me.AxEpsonFPHostControl1.Location = New System.Drawing.Point(549, 38)
        Me.AxEpsonFPHostControl1.Name = "AxEpsonFPHostControl1"
        Me.AxEpsonFPHostControl1.OcxState = CType(resources.GetObject("AxEpsonFPHostControl1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxEpsonFPHostControl1.Size = New System.Drawing.Size(29, 29)
        Me.AxEpsonFPHostControl1.TabIndex = 0
        '
        'btn_no_venta
        '
        Me.btn_no_venta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_no_venta.Location = New System.Drawing.Point(105, 12)
        Me.btn_no_venta.Name = "btn_no_venta"
        Me.btn_no_venta.Size = New System.Drawing.Size(87, 23)
        Me.btn_no_venta.TabIndex = 13
        Me.btn_no_venta.Text = "No Venta"
        Me.btn_no_venta.UseVisualStyleBackColor = True
        '
        'btn_libro_de_venta
        '
        Me.btn_libro_de_venta.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_libro_de_venta.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_libro_de_venta.Location = New System.Drawing.Point(198, 12)
        Me.btn_libro_de_venta.Name = "btn_libro_de_venta"
        Me.btn_libro_de_venta.Size = New System.Drawing.Size(182, 23)
        Me.btn_libro_de_venta.TabIndex = 12
        Me.btn_libro_de_venta.Text = "Libro de venta"
        Me.btn_libro_de_venta.UseVisualStyleBackColor = True
        '
        'btn_reconectar
        '
        Me.btn_reconectar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_reconectar.Location = New System.Drawing.Point(12, 12)
        Me.btn_reconectar.Name = "btn_reconectar"
        Me.btn_reconectar.Size = New System.Drawing.Size(87, 23)
        Me.btn_reconectar.TabIndex = 11
        Me.btn_reconectar.Text = "Reconectar"
        Me.btn_reconectar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.btn_fact_con_exoneracion_de_impuestos)
        Me.GroupBox1.Controls.Add(Me.btn_fact_credito_fiscal)
        Me.GroupBox1.Controls.Add(Me.cmd_fact_consumidor_final)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 41)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(368, 138)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Facturación"
        '
        'btn_fact_con_exoneracion_de_impuestos
        '
        Me.btn_fact_con_exoneracion_de_impuestos.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_fact_con_exoneracion_de_impuestos.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_fact_con_exoneracion_de_impuestos.Location = New System.Drawing.Point(6, 95)
        Me.btn_fact_con_exoneracion_de_impuestos.Name = "btn_fact_con_exoneracion_de_impuestos"
        Me.btn_fact_con_exoneracion_de_impuestos.Size = New System.Drawing.Size(356, 29)
        Me.btn_fact_con_exoneracion_de_impuestos.TabIndex = 2
        Me.btn_fact_con_exoneracion_de_impuestos.Text = "Factura con exoneración de impuestos"
        Me.btn_fact_con_exoneracion_de_impuestos.UseVisualStyleBackColor = True
        '
        'btn_fact_credito_fiscal
        '
        Me.btn_fact_credito_fiscal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_fact_credito_fiscal.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_fact_credito_fiscal.Location = New System.Drawing.Point(6, 60)
        Me.btn_fact_credito_fiscal.Name = "btn_fact_credito_fiscal"
        Me.btn_fact_credito_fiscal.Size = New System.Drawing.Size(356, 29)
        Me.btn_fact_credito_fiscal.TabIndex = 1
        Me.btn_fact_credito_fiscal.Text = "Factura crédito fiscal"
        Me.btn_fact_credito_fiscal.UseVisualStyleBackColor = True
        '
        'cmd_fact_consumidor_final
        '
        Me.cmd_fact_consumidor_final.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmd_fact_consumidor_final.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmd_fact_consumidor_final.Location = New System.Drawing.Point(6, 25)
        Me.cmd_fact_consumidor_final.Name = "cmd_fact_consumidor_final"
        Me.cmd_fact_consumidor_final.Size = New System.Drawing.Size(356, 29)
        Me.cmd_fact_consumidor_final.TabIndex = 0
        Me.cmd_fact_consumidor_final.Text = "Factura consumidor final"
        Me.cmd_fact_consumidor_final.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.btn_nc_exoneracion)
        Me.GroupBox2.Controls.Add(Me.btn_nc_credito_fiscal)
        Me.GroupBox2.Controls.Add(Me.btn_nc_consumidor_final)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 185)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(368, 138)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Notas de crédito"
        '
        'btn_nc_exoneracion
        '
        Me.btn_nc_exoneracion.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_nc_exoneracion.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Italic)
        Me.btn_nc_exoneracion.Location = New System.Drawing.Point(6, 95)
        Me.btn_nc_exoneracion.Name = "btn_nc_exoneracion"
        Me.btn_nc_exoneracion.Size = New System.Drawing.Size(356, 29)
        Me.btn_nc_exoneracion.TabIndex = 2
        Me.btn_nc_exoneracion.Text = "Nota de crédito con exoneración"
        Me.btn_nc_exoneracion.UseVisualStyleBackColor = True
        '
        'btn_nc_credito_fiscal
        '
        Me.btn_nc_credito_fiscal.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_nc_credito_fiscal.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Italic)
        Me.btn_nc_credito_fiscal.Location = New System.Drawing.Point(6, 60)
        Me.btn_nc_credito_fiscal.Name = "btn_nc_credito_fiscal"
        Me.btn_nc_credito_fiscal.Size = New System.Drawing.Size(356, 29)
        Me.btn_nc_credito_fiscal.TabIndex = 1
        Me.btn_nc_credito_fiscal.Text = "Nota de crédito a crédito fiscal"
        Me.btn_nc_credito_fiscal.UseVisualStyleBackColor = True
        '
        'btn_nc_consumidor_final
        '
        Me.btn_nc_consumidor_final.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_nc_consumidor_final.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Italic)
        Me.btn_nc_consumidor_final.Location = New System.Drawing.Point(6, 25)
        Me.btn_nc_consumidor_final.Name = "btn_nc_consumidor_final"
        Me.btn_nc_consumidor_final.Size = New System.Drawing.Size(356, 29)
        Me.btn_nc_consumidor_final.TabIndex = 0
        Me.btn_nc_consumidor_final.Text = "Nota de crédito a consumidor final"
        Me.btn_nc_consumidor_final.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox3.Controls.Add(Me.btn_cierre_z)
        Me.GroupBox3.Controls.Add(Me.btn_informe_x)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(12, 329)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(368, 105)
        Me.GroupBox3.TabIndex = 16
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Reportes"
        '
        'btn_cierre_z
        '
        Me.btn_cierre_z.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_cierre_z.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Italic)
        Me.btn_cierre_z.Location = New System.Drawing.Point(6, 60)
        Me.btn_cierre_z.Name = "btn_cierre_z"
        Me.btn_cierre_z.Size = New System.Drawing.Size(356, 29)
        Me.btn_cierre_z.TabIndex = 1
        Me.btn_cierre_z.Text = "Cierre Z"
        Me.btn_cierre_z.UseVisualStyleBackColor = True
        '
        'btn_informe_x
        '
        Me.btn_informe_x.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_informe_x.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Italic)
        Me.btn_informe_x.Location = New System.Drawing.Point(6, 25)
        Me.btn_informe_x.Name = "btn_informe_x"
        Me.btn_informe_x.Size = New System.Drawing.Size(356, 29)
        Me.btn_informe_x.TabIndex = 0
        Me.btn_informe_x.Text = "Informe X"
        Me.btn_informe_x.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox4.Controls.Add(Me.btn_por_fecha)
        Me.GroupBox4.Controls.Add(Me.btn_por_numero)
        Me.GroupBox4.Controls.Add(Me.DateTimePicker2)
        Me.GroupBox4.Controls.Add(Me.DateTimePicker1)
        Me.GroupBox4.Controls.Add(Me.txt_z_1)
        Me.GroupBox4.Controls.Add(Me.txt_z_2)
        Me.GroupBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox4.Location = New System.Drawing.Point(12, 440)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(368, 89)
        Me.GroupBox4.TabIndex = 17
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Auditoria"
        '
        'btn_por_fecha
        '
        Me.btn_por_fecha.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_por_fecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btn_por_fecha.Location = New System.Drawing.Point(257, 53)
        Me.btn_por_fecha.Name = "btn_por_fecha"
        Me.btn_por_fecha.Size = New System.Drawing.Size(105, 23)
        Me.btn_por_fecha.TabIndex = 8
        Me.btn_por_fecha.Text = "Por fecha"
        Me.btn_por_fecha.UseVisualStyleBackColor = True
        '
        'btn_por_numero
        '
        Me.btn_por_numero.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btn_por_numero.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btn_por_numero.Location = New System.Drawing.Point(257, 28)
        Me.btn_por_numero.Name = "btn_por_numero"
        Me.btn_por_numero.Size = New System.Drawing.Size(105, 23)
        Me.btn_por_numero.TabIndex = 7
        Me.btn_por_numero.Text = "Por Número"
        Me.btn_por_numero.UseVisualStyleBackColor = True
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker2.Location = New System.Drawing.Point(130, 55)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(117, 20)
        Me.DateTimePicker2.TabIndex = 6
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(6, 55)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(117, 20)
        Me.DateTimePicker1.TabIndex = 5
        '
        'txt_z_1
        '
        Me.txt_z_1.Location = New System.Drawing.Point(6, 29)
        Me.txt_z_1.Name = "txt_z_1"
        Me.txt_z_1.Size = New System.Drawing.Size(117, 20)
        Me.txt_z_1.TabIndex = 4
        '
        'txt_z_2
        '
        Me.txt_z_2.Location = New System.Drawing.Point(130, 29)
        Me.txt_z_2.Name = "txt_z_2"
        Me.txt_z_2.Size = New System.Drawing.Size(117, 20)
        Me.txt_z_2.TabIndex = 2
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ProgressBar1.Location = New System.Drawing.Point(0, 544)
        Me.ProgressBar1.MarqueeAnimationSpeed = 25
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(392, 15)
        Me.ProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.ProgressBar1.TabIndex = 18
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(392, 559)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btn_no_venta)
        Me.Controls.Add(Me.btn_libro_de_venta)
        Me.Controls.Add(Me.btn_reconectar)
        Me.Controls.Add(Me.AxEpsonFPHostControl1)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "POS_fiscal"
        CType(Me.AxEpsonFPHostControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents AxEpsonFPHostControl1 As AxEpsonFPHostControlX.AxEpsonFPHostControl
    Friend WithEvents btn_no_venta As Button
    Friend WithEvents btn_libro_de_venta As Button
    Friend WithEvents btn_reconectar As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents btn_fact_con_exoneracion_de_impuestos As Button
    Friend WithEvents btn_fact_credito_fiscal As Button
    Friend WithEvents cmd_fact_consumidor_final As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents btn_nc_exoneracion As Button
    Friend WithEvents btn_nc_credito_fiscal As Button
    Friend WithEvents btn_nc_consumidor_final As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents btn_cierre_z As Button
    Friend WithEvents btn_informe_x As Button
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents btn_por_fecha As Button
    Friend WithEvents btn_por_numero As Button
    Friend WithEvents DateTimePicker2 As DateTimePicker
    Friend WithEvents DateTimePicker1 As DateTimePicker
    Friend WithEvents txt_z_1 As TextBox
    Friend WithEvents txt_z_2 As TextBox
    Friend WithEvents ProgressBar1 As ProgressBar
End Class
